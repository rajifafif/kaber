<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/theme.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>

<body>
    <!-- WRAPPER -->
    <div id="wrapper">

        <!-- NAVBAR -->
        <nav class="navbar navbar-expand fixed-top">
            <div class="navbar-brand d-none d-lg-block">
                <a href="index.html"><img src="{{ asset('images/logo-white.png') }}" alt="Klorofil Pro Logo"
                        class="img-fluid logo"></a>
            </div>
            <div class="container-fluid p-0">
                <button id="tour-fullwidth" type="button" class="btn btn-default btn-toggle-fullwidth"><i
                        class="ti-menu"></i></button>
                <form class="form-inline search-form mr-auto d-none d-sm-block">
                    <div class="input-group">
                        <input type="text" value="" class="form-control" placeholder="Search dashboard...">
                        <div class="input-group-append">
                            <button type="button" class="btn"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
                <div id="navbar-menu">
                    <ul class="nav navbar-nav align-items-center">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img
                                    src="https://i.imgur.com/M701HZb.jpg" class="user-picture" alt=" Avatar">
                                <span>Samuel</span></a>
                            <ul class="dropdown-menu dropdown-menu-right logged-user-menu">
                                <li><a href="#"><i class="ti-user"></i> <span>My Profile</span></a></li>
                                <li><a href="appviews-inbox.html"><i class="ti-email"></i> <span>Message</span></a></li>
                                <li><a href="#"><i class="ti-settings"></i> <span>Settings</span></a></li>
                                <li><a href="page-lockscreen.html"><i class="ti-power-off"></i> <span>Logout</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- END NAVBAR -->

        <!-- LEFT SIDEBAR -->
        <div id="sidebar-nav" class="sidebar">
            <nav>
                <ul class="nav" id="sidebar-nav-menu">
                    <li class="menu-group">Main</li>
                    <li class="panel">
                        <a href="#dashboards" data-toggle="collapse" data-parent="#sidebar-nav-menu" aria-expanded=""
                            class=""><i class="ti-dashboard"></i> <span class="title">Dashboards</span> <i
                                class="icon-submenu ti-angle-left"></i></a>
                        <div id="dashboards" class="collapse  ">
                            <ul class="submenu">
                                <li><a href="index.html" class="">Dashboard v1</a></li>
                                <li><a href="dashboard2.html" class="">Dashboard v2</a></li>
                                <li><a href="dashboard3.html" class="">Dashboard v3</a></li>
                                <li><a href="dashboard4.html" class="">Dashboard v4</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
                <button type="button" class="btn-toggle-minified" title="Toggle Minified Menu"><i
                        class="ti-arrows-horizontal"></i></button>
            </nav>
        </div>
        <!-- END LEFT SIDEBAR -->

        <!-- MAIN -->
        <div class="main">

            <!-- MAIN CONTENT -->
            <div class="main-content">

                <div class="content-heading">
                    <div class="heading-left">
                        <h1 class="page-title">Layout Default</h1>
                        <p class="page-subtitle">Click the thumbnail to see other layouts.</p>
                    </div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Parent</a></li>
                            <li class="breadcrumb-item active">Current</li>
                        </ol>
                    </nav>
                </div>

                <!-- APP VUE-->
                <div id="app">
                <div class="container-fluid">

                    @yield('content')

                    <div class="row mt-5 demo-layout">
                    </div>
                </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->

        </div>
        <!-- END MAIN -->

        <div class="clearfix"></div>

        <!-- footer -->
        <footer>
            <div class="container-fluid">
                <p class="copyright">&copy; 2020 <a href="https://www.themeineed.com" target="_blank">Theme I Need</a>.
                    All Rights Reserved.</p>
            </div>
        </footer>
        <!-- end footer -->

    </div>
    <!-- END WRAPPER -->

    <!-- Scripts -->
    <script src="{{ asset('js/theme.js') }}" defer></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>

</html>
