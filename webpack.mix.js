const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .scripts([
        'theme/assets/js/vendor.min.js',
        'theme/assets/plugins/jquery-jeditable/jquery.jeditable.min.js',
        'theme/assets/plugins/jquery.maskedinput/jquery.maskedinput.js',
        'theme/assets/plugins/jquery-jeditable/jquery.jeditable.masked.min.js',
        'theme/assets/plugins/jquery-ui/jquery-ui.min.js',
        'theme/assets/plugins/jquery-jeditable/jquery.jeditable.datepicker.min.js',
        'theme/assets/plugins/jqvmap/jquery.vmap.min.js',
        'theme/assets/plugins/jqvmap/maps/jquery.vmap.world.js',
        'theme/assets/plugins/jqvmap/maps/jquery.vmap.usa.js',
        'theme/assets/plugins/flot/jquery.canvaswrapper.js',
        'theme/assets/plugins/flot/jquery.colorhelpers.js',
        'theme/assets/plugins/flot/jquery.flot.js',
        'theme/assets/plugins/flot/jquery.flot.saturated.js',
        'theme/assets/plugins/flot/jquery.flot.browser.js',
        'theme/assets/plugins/flot/jquery.flot.drawSeries.js',
        'theme/assets/plugins/flot/jquery.flot.uiConstants.js',
        'theme/assets/plugins/flot/jquery.flot.resize.js',
        'theme/assets/plugins/flot/jquery.flot.legend.js',
        'theme/assets/plugins/flot/jquery.flot.hover.js',
        'theme/assets/plugins/flot/jquery.flot.time.js',
        'theme/assets/plugins/jquery.flot.tooltip/jquery.flot.tooltip.min.js',
        'theme/assets/plugins/justgage/raphael.min.js',
        'theme/assets/plugins/justgage/justgage.min.js',
        'theme/assets/plugins/datatables.net/jquery.dataTables.min.js',
        'theme/assets/plugins/datatables.net-bs4/dataTables.bootstrap4.min.js',
        'theme/assets/plugins/jquery.appear/jquery.appear.js',
        'theme/assets/plugins/jquery-sparkline/jquery.sparkline.min.js',
        'theme/assets/plugins/jquery-ui/jquery-ui.min.js',
        'theme/assets/plugins/bootstrap-tour/bootstrap-tour-standalone.js',
        'theme/assets/js/app.min.js'
    ], 'public/js/theme.js')
    .sass('resources/sass/app.scss', 'public/css')
    .styles([
        'theme/assets/plugins/jquery-ui/jquery-ui.min.css',
        'theme/assets/plugins/jqvmap/jqvmap.min.css',
        'theme/assets/css/bootstrap-custom.css',
        'theme/assets/css/app.css'
    ], 'public/css/theme.css');
    // .copyDirectory('theme/assets/fonts', 'public/fonts')
    // .copyDirectory('theme/assets/images', 'public/images');